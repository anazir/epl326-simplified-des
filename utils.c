#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"

const int S1[4][4] = { {1, 0, 3, 2}, {3, 2, 1, 0}, {0, 2, 1, 3}, {3, 1, 3, 2} };
const int S2[4][4] = { {0, 1, 2, 3}, {2, 0, 1, 3}, {3, 0, 1, 0}, {2, 1, 0, 3} };

char* permutation(char *str, int* order, int size) {
  char *new_str = calloc(size, sizeof(char));

  if (!new_str) {
    fprintf(stderr, "Memory allocation failed!\n");
  }

  for (int i = 0; i < size; i++) {
    new_str[i] = str[order[i]];
  }

  return new_str;
}

char* xor_str(char *str1, char *str2) {
  char *new_str = calloc(strlen(str1), sizeof(char));

  if (!new_str) {
    fprintf(stderr, "Memory allocation failed!\n");
  }

  for (int i = 0; i < strlen(str1); i++) {
    new_str[i] = ((str1[i] == '1') ^ (str2[i] == '1')) ? '1' : '0';
  }

  return new_str;
}

void split_str(char *str, char* left, char *right) {
  int half_len = strlen(str) / 2;
  strncpy(left, str, half_len);
  strncpy(right, str + half_len, half_len);
  left[half_len] = '\0';
  right[half_len] = '\0';
}

int get_from_sbox(char* str, const int sbox[][4]) {
  char x[3] = { str[0], str[3], '\0' };
  char y[3] = { str[1], str[2], '\0' };

  return sbox[strtol(x, NULL, 2)][strtol(y, NULL, 2)];
}

char* dec_to_bin(int n, int len) {
  char *padding = calloc(len, sizeof(char));

  if (!padding) {
    fprintf(stderr, "Memory allocation failed!\n");
  }

  for (int i = 0; i < len - 1; i++) {
    padding[i] = '0';
  }

  if (n == 0) {
    padding[len - 1] = '0';
    return padding;
  }

  if (n == 1) {
    padding[len - 1] = '1';
    return padding;
  }

  char *first = dec_to_bin(n / 2, len - 1);
  char *second = dec_to_bin(n % 2, 1);
  strcat(first, second);

  free(second);
  free(padding);

  return first;
}

char* merge_strings(char* str1, char* str2, int order[], int size) {
  char *new_str = calloc(size, sizeof(char));

  if (!new_str) {
    fprintf(stderr, "Memory allocation failed!\n");
  }

  for (int i = 0; i < size; i++) {
    char cur = str1[order[i] % strlen(str1)];

    if (order[i] >= strlen(str1)) {
      cur = str2[order[i] - strlen(str1)];
    }

    new_str[i] = cur;
  }

  return new_str;
}
