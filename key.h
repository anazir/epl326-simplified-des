#ifndef KEY_H
#define KEY_H

void left_shift(char* key, int n);
char* merge_key(char* left, char* right);

char** generate_keys(char* key);

#endif // !KEY_H
