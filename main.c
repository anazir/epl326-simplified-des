#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "key.h"
#include "utils.h"

char* encryption_round(char* left, char* right, char* key) {
  // Expand right side
  char* ep = permutation(right, (int []){3, 0, 1, 2, 1, 2, 3, 0}, 8);

  char* xor_ep = xor_str(ep, key);

  char ep_left[5], ep_right[5];
  split_str(xor_ep, ep_left, ep_right);

  int s0 = get_from_sbox(ep_left, S1);
  int s1 = get_from_sbox(ep_right, S2);

  char* str0 = dec_to_bin(s0, 2);
  char* str1 = dec_to_bin(s1, 2);

  char* merge_s = merge_strings(str0, str1, (int []){1, 3, 2, 0}, 4);

  char* result = xor_str(left, merge_s);

  free(merge_s);
  free(str0);
  free(str1);
  free(ep);
  free(xor_ep);

  return result;
}

char* encrypt(char* plaintext, char* key) {
  char** keys = generate_keys(key);

  // Initial permutation
  char* ip = permutation(plaintext, (int []){1, 5, 2, 0, 3, 7, 4, 6}, 8);

  // Split to left and right
  char left[5], right[5];
  split_str(ip, left, right);

  char* res1 = encryption_round(left, right, keys[0]);
  char* res2 = encryption_round(right, res1, keys[1]);

  char* ciphertext = merge_strings(res2, res1, (int []){ 3, 0, 2, 4, 6, 1, 7, 5 }, 8);

  free(keys[0]);
  free(keys[1]);
  free(keys);
  free(res1);
  free(res2);
  free(ip);

  return ciphertext;
}

int main (int argc, char *argv[]) {
  char plaintext[9], key[11];

  if (argc == 1) {
    strcpy(plaintext, "01010001");
    strcpy(key, "0101001100");
  } else if (argc == 3) {
    strcpy(plaintext, argv[1]);
    strcpy(key, argv[2]);
  }

  char* ciphertext = encrypt(plaintext, key);
  printf("%s\n", ciphertext);

  free(ciphertext);

  return 0;
}
