#ifndef UTILS_H
#define UTILS_H

// S-Boxes
extern const int S1[4][4];
extern const int S2[4][4];

char* permutation(char *str, int* order, int size);
char* xor_str(char *str1, char *str2);
void split_str(char *str, char* left, char *right);
int get_from_sbox(char* str, const int sbox[][4]);
char* dec_to_bin(int n, int len);
char* merge_strings(char* str1, char* str2, int order[], int size);

#endif /* !UTILS_H */

