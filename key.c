#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"

void left_shift(char* key, int n) {
  char *new_key = calloc(strlen(key), sizeof(char));

  if (!new_key) {
    fprintf(stderr, "Memory allocation failed!\n");
  }

  for (int i = 0; i < strlen(key); i++) {
    new_key[i] = key[(i + n) % strlen(key)];
  }

  strcpy(key, new_key);
  free(new_key);
}

char* merge_key(char* left, char* right) {
  return merge_strings(left, right, (int []){ 5, 2, 6, 3, 7, 4, 9, 8 }, 8);
}

char** generate_keys(char* key) {
  char* *keys = calloc(2, sizeof(char*));

  if (!keys) {
    fprintf(stderr, "Memory allocation failed!\n");
  }

  char* shuffled_key = permutation(key, (int []){ 2, 4, 1, 6, 3, 9, 0, 8, 7, 5 }, 10);

  char left[6], right[6];
  split_str(shuffled_key, left, right);

  left_shift(left, 1);
  left_shift(right, 1);

  keys[0] = merge_key(left, right);

  left_shift(left, 2);
  left_shift(right, 2);

  keys[1] = merge_key(left, right);

  free(shuffled_key);

  return keys;
}
